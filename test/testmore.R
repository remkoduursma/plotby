library(HIEv)  
library(plotBy)
# for either, do this first if you have to:
# library(devtools)
# install_bitbucket("plotby","remkoduursma")
# install_bitbucket("hiev","remkoduursma")

dfr <- downloadTOA5("WTC[0-9]{1,2}_Table1", maxnfiles=200, topath="c:/tmp")
dfr$chamber <- paste0("ch", str_extract(dfr$Source, "[0-9]{2}"))

palette(c("blue","red","forestgreen"))

pdf("WTC_soilwater_bychamberdepth.pdf")
plotBy(VW_Avg.1. + VW_Avg.2. + VW_Avg.3. ~ DateTime | chamber, 
       how = "panel",
       data=dfr, type='l', legend=TRUE, legendwhere="topright",
       ylab=expression("Soil water content"~~(m^3~m^-3)))
dev.off()



pdf("tester.pdf")
plotBy(VW_Avg.1. + VW_Avg.2. + VW_Avg.3. ~ DateTime | chamber, 
       how = "panel",
       data=dfr, type='l', legend=TRUE, legendwhere="topright-1",
       ylab=expression("Soil water content"~~(m^3~m^-3)))
dev.off()


# Split by chamber, colour by chamber, one variable only.
windows(10,10)
plotBy(VW_Avg.2. ~ DateTime | chamber, 
       how = "colour",
       data=dfr, type='l', legend=FALSE, 
       ylab=expression("Soil water content"~~(m^3~m^-3)))


# Split by group, colour same variable in same colour for each group.
# Might be handy some times.
# But how do we make colour by chamber, not by variable? Can be easy setting I suppose...
# col=chamber ? 
dfr2 <- subset(dfr, chamber %in% c("ch04","ch05"))

windows(10,10)
plotBy(VW_Avg.1. + VW_Avg.2. ~ DateTime | chamber, 
       how = "colour",
       data=dfr2, type='l', legend=FALSE, 
       ylab=expression("Soil water content"~~(m^3~m^-3)))




# palette(c("blue","red","forestgreen"))
# 
# plotme <- function(ch){
#   
#   dat <- subset(dfr, chamber == ch)
#   
#   plotBy(VW_Avg.1. + VW_Avg.2. + VW_Avg.3. ~ DateTime, 
#          data=dat, type='l', legend=TRUE, main=ch, legendwhere="topright",
#          ylab=expression("Soil water content"~~(m^3~m^-3)))
# }
# 
# pdf("VWC by chamber.pdf")
# for(x in unique(dfr$chamber))plotme(x)
# dev.off()
