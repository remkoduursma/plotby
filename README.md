The plotBy R package
--------------------

This package provides an extended formula interface for the base `plot` function. It is a fairly experimental package.

To install,

```
library(devtools)
install_bitbucket("plotBy", "remkoduursma")
```

Windows users must have [Rtools](http://cran.at.r-project.org/bin/windows/Rtools/) installed. 